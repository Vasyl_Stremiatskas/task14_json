package com.stremiatskas;

public class Blade {
    private double length;
    private double width;

    public Blade() {
    }

    public Blade(double length, double width) {
        this.length = length;
        this.width = width;
    }

    public double getLength() {
        return length;
    }

    public void setLength(double length) {
        this.length = length;
    }

    public double getWidth() {
        return width;
    }

    public void setWidth(double width) {
        this.width = width;
    }

    @Override
    public String toString() {
        return "Blade{" +
                "length=" + length +
                ", width=" + width +
                '}';
    }
}