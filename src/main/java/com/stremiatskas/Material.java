package com.stremiatskas;

public class Material {
    private String edge;
    private String handle;

    public Material() {
    }

    public Material(String edge, String handle) {
        this.edge = edge;
        this.handle = handle;
    }

    public String getEdge() {
        return edge;
    }

    public void setEdge(String edge) {
        this.edge = edge;
    }

    public String getHandle() {
        return handle;
    }

    public void setHandle(String handle) {
        this.handle = handle;
    }

    @Override
    public String toString() {
        return "Material{" +
                "edge='" + edge + '\'' +
                ", handle=" + handle +
                '}';
    }
}