package com.stremiatskas;

public class Visual {
    private Blade blade;
    private Material material;
    private boolean bloodstream;

    public Visual() {
    }

    public Visual(Blade blade, Material material, boolean bloodstream) {
        this.blade = blade;
        this.material = material;
        this.bloodstream = bloodstream;
    }

    public Blade getBlade() {
        return blade;
    }

    public void setBlade(Blade blade) {
        this.blade = blade;
    }

    public Material getMaterial() {
        return material;
    }

    public void setMaterial(Material material) {
        this.material = material;
    }

    public boolean isBloodstream() {
        return bloodstream;
    }

    public void setBloodstream(boolean bloodstream) {
        this.bloodstream = bloodstream;
    }

    @Override
    public String toString() {
        return "Visual{" +
                "blade=" + blade +
                ", material=" + material +
                ", bloodstream=" + bloodstream +
                '}';
    }
}