package com.stremiatskas;

import org.everit.json.schema.Schema;
import org.everit.json.schema.ValidationException;
import org.everit.json.schema.loader.SchemaLoader;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.io.IOException;
import java.io.InputStream;

public class Validator_JSON {
    public void validateJson(){
        try (InputStream inputStream = getClass().getResourceAsStream("src/main/resources/knifeJSONScheme.json")) {
            JSONObject rawSchema = new JSONObject(new JSONTokener(inputStream));
            Schema vschema = SchemaLoader.load(rawSchema);
            vschema.validate(new JSONObject(("{\"validation\" : \"obj\"}")));
        } catch (ValidationException e) {
            System.out.println("Validation exception");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}