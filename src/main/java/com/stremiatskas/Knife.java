package com.stremiatskas;

public class Knife {
    private int knifesID;
    private String type;
    private boolean one_handed;
    private String origin;
    private boolean value;
    private Visual visual;

    public Knife() {
    }

    public Knife(int knifesID, String type, boolean one_handed, String origin, boolean value, Visual visual) {
        this.knifesID = knifesID;
        this.type = type;
        this.one_handed = one_handed;
        this.origin = origin;
        this.value = value;
        this.visual = visual;
    }

    public int getKnifesID() {
        return knifesID;
    }

    public void setKnifesID(int knifesID) {
        this.knifesID = knifesID;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public boolean isOne_handed() {
        return one_handed;
    }

    public void setOne_handed(boolean one_handed) {
        this.one_handed = one_handed;
    }

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public boolean isValue() {
        return value;
    }

    public void setValue(boolean value) {
        this.value = value;
    }

    public Visual getVisual() {
        return visual;
    }

    public void setVisual(Visual visual) {
        this.visual = visual;
    }

    @Override
    public String toString() {
        return "Knife{" +
                "type='" + type + '\'' +
                ", one_handed=" + one_handed +
                ", origin='" + origin + '\'' +
                ", value=" + value +
                ", visual=" + visual +
                '}';
    }
}