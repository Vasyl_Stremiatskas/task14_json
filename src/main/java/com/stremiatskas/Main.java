package com.stremiatskas;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import org.everit.json.schema.Schema;
import org.everit.json.schema.ValidationException;
import org.everit.json.schema.loader.SchemaLoader;
import org.json.JSONObject;
import org.json.JSONTokener;

public class Main {

        public static void main(String[] args) {
            File json = new File("src/main/resources/knifeJSON.json");
            File schema = new File("src/main/resources/knifeJSONScheme.json");

            Validator_JSON v = new Validator_JSON();
            v.validateJson();

            Parser_JSON parser = new Parser_JSON();

            printList(parser.getKnifeList(json));
        }

        private static void printList(List<Knife> knifes) {
            System.out.println("JSON");
            for (Knife knife : knifes) {
                System.out.println(knife);
            }
        }
    }