package com.stremiatskas;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

public class Parser_JSON {
    private ObjectMapper objectMapper;

    public Parser_JSON() {
        this.objectMapper = new ObjectMapper();
    }

    public List<Knife> getKnifeList(File jsonFile){
        Knife[] knifes = new Knife[1];
        try{
            knifes = objectMapper.readValue(jsonFile, Knife[].class);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return Arrays.asList(knifes);
    }
}